#. extracted from content/editions/server/home.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-09-21 10:26+0000\n"
"Last-Translator: Hoppár Zoltán <hopparz@gmail.com>\n"
"Language-Team: Hungarian <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsserverhome/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "index"
msgstr ""

#: title
msgid "The leading edge server"
msgstr "Az élvonalbeli kiszolgáló"

#: description
msgid ""
"Run applications on bare metal or the cloud with a Linux server OS packed "
"with the latest open source technology\n"
msgstr ""

#: header_images-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/server_main.png"
msgstr ""

#: header_images-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/iot_background.jpg"
msgstr ""

#: links-%3E[0]-%3Etext
msgid "Download Now"
msgstr "Töltse le most"

#: links-%3E[0]-%3Eurl
#, read-only
msgid "https://getfedora.org/en/server/download/"
msgstr ""

#~ msgid "Fedora Server Index"
#~ msgstr "Fedora Szerver Index"

#~ msgid ""
#~ "Run applications on bare metal or the cloud with a Linux server OS packed "
#~ "with the latest open source technology"
#~ msgstr ""
#~ "Futtassa az alkalmazásokat magán a hardveren vagy felhőben a legújabb nyílt "
#~ "forráskódú technológiával ellátott Linux szerver operációs rendszerrel"
